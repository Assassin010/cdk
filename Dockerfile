FROM zenika/terraform-aws-cli:latest

USER root

# Fix permissions for apt
RUN mkdir -p /var/lib/apt/lists/partial \
  && chmod -R 644 /var/lib/apt/lists \
  && chmod 755 /var/lib/apt/lists/partial

# Update package sources and install Python 3, python3-venv, Docker, Node.js, and npm
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg \
     lsb-release \
     python3 \
     python3-pip \
     python3-venv \
     nodejs \
     npm \
  && curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg \
  && echo "deb [signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian buster stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null \
  && apt-get update \
  && apt-get install -y --no-install-recommends docker-ce docker-ce-cli containerd.io \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Install AWS CDK
RUN npm install -g aws-cdk

# Add Python 3 to PATH
RUN echo 'export PATH="$PATH:/usr/bin/python3"' >> /home/nonroot/.bashrc

# Verify Python, pip, and npm installation
RUN python3 --version && pip3 --version && npm --version

# Specify the default command to run on container start
CMD ["bash"]

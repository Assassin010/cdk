variable "aws_region" {
  default = "eu-west-1"
}

variable "instance_user_data" {
  type    = string
  default = <<EOF
              #!/bin/bash
              sudo yum update -y
              sudo yum install -y openssh-server
              sudo systemctl enable sshd
              sudo systemctl start sshd
            EOF
}
output "iam_role_name" {
  description = "The name of the IAM role created by the module"
  value       = aws_iam_role.this.name
}

output "instance_profile_name" {
  description = "The name of the IAM instance profile created by the module"
  value       = aws_iam_instance_profile.this.name
}

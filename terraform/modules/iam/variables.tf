variable "role_name" {
  description = "The name of the IAM role"
  type        = string
}

variable "policy_name" {
  description = "The name of the IAM policy"
  type        = string
}


variable "instance_profile_name" {
  description = "The name of the IAM instance profile"
  type        = string
}

variable "prefix" {
  description = "The prefix to use for the IAM resources"
  type        = string
}

# variable "ec2_instance_id" {
#   description = "The ID of the EC2 instance to associate with the instance profile"
#   type      = string
# }
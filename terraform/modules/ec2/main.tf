
#------------------------------------------------------------------------------
# Instances
#------------------------------------------------------------------------------
resource "aws_instance" "ec2_instances" {
  for_each = var.instances

  ami                         = each.value.ami
  instance_type               = each.value.instance_type
  iam_instance_profile        = each.value.iam_instance_profile
  key_name                    = each.value.key_name
  subnet_id                   = each.value.subnet_id
  user_data                   = each.value.user_data
  vpc_security_group_ids      = [each.value.security_group_id]
  associate_public_ip_address = each.value.associate_public_ip_address
  tags = {
    Name = each.value.tag_name
  }
}


resource "null_resource" "install_script" {
  for_each = { for k, v in var.instances : k => v if k == "master_instance" }

  triggers = {
    instance_id = aws_instance.ec2_instances[each.key].id
  }

  provisioner "file" {
    source      = "${path.module}/run_provisioner/install.sh"
    destination = "/tmp/install.sh"

    connection {
      type        = "ssh"
      user        = "ec2-user" # Adjust based on your AMI
      private_key = file("${path.module}/key/cdk-eu-west-1.pem")
      host        = aws_instance.ec2_instances[each.key].public_ip
    }
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/install.sh",
      "/tmp/install.sh",
    ]

    connection {
      type        = "ssh"
      user        = "ec2-user" # Adjust based on your AMI
      private_key = file("${path.module}/key/cdk-eu-west-1.pem")
      host        = aws_instance.ec2_instances[each.key].public_ip
    }
  }
}




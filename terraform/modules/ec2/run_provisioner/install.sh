#!/bin/bash

sudo yum update -y

# Check if jq is installed
if command -v jq &> /dev/null; then
    # jq is installed, print version
    jq --version
else
    # jq is not installed, install it
    echo "jq is not installed. Installing..."
    sudo yum install -y jq
fi

# Check if AWS CLI is installed
if command -v aws &> /dev/null; then
    # awscli is installed, print version
    echo "AWS CLI version: $(aws --version)"
else
    # awscli is not installed, install the latest version
    echo "AWS CLI is not installed. Installing..."
    sudo yum install -y awscli
fi

# Check if Python 3 and pip3 are installed
if command -v python3 &> /dev/null && command -v pip3 &> /dev/null; then
    # Python 3 and pip3 are installed, print versions
    echo "Python version: $(python3 --version)"
    echo "pip version: $(pip3 --version)"
else
    # Python 3 or pip3 is not installed, install both
    echo "Python 3 or pip3 is not installed. Installing..."
    sudo yum install -y python3
    sudo yum install -y python3-pip
fi

# Install boto3 and botocore
sudo pip3 install boto3
sudo pip3 install botocore

# Install Ansible
sudo yum install -y ansible
# Check if Ansible is installed
ansible --version
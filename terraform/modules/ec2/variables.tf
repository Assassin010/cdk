variable "instances" {
  description = "A map of instance configurations"
  type = map(object({
    ami                         = string
    instance_type               = string
    iam_instance_profile        = string
    key_name                    = string
    subnet_id                   = string
    security_group_id           = string
    user_data                   = string
    associate_public_ip_address = bool # New attribute for auto-assign public IP
    tag_name                    = string
  }))
}


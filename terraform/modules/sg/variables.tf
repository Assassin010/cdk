# S3 bucket
variable "prefix" {
  description = "The prefix to use for the S3 bucket"
  type        = string
}


# VPC
variable "vpc_id" {
  description = "The VPC ID"
  type        = string
}


# Security Group master instance
variable "sg_name" {
  description = "security_group name"
  type        = string
}


variable "ingress_rules" {
  description = "List of ingress rules for the security group"
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
  }))
}

variable "egress_rules" {
  description = "List of egress rules for the security group"
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
  }))
}



# Security Group private instance
variable "private_sg_name" {
  description = "security_group name"
  type        = string
}

variable "private_ingress_rules" {
  description = "List of ingress rules for the security group"
  type = list(object({
    from_port       = number
    to_port         = number
    protocol        = string
    security_groups = list(string)
  }))
}


variable "private_egress_rules" {
  description = "List of egress rules for the security group"
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
  }))
}
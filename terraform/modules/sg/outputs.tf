output "security_group_id" {
  value = aws_security_group.this.id
}

output "private_security_group_id" {
  value = aws_security_group.private.id
}
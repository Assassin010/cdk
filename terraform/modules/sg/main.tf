#------------------------------------------------------------------------------
# Default Security Group
#------------------------------------------------------------------------------
resource "aws_security_group" "this" {
  name        = "${var.prefix}-${var.sg_name}"
  description = "Example security group"
  vpc_id      = var.vpc_id
  dynamic "ingress" {
    for_each = var.ingress_rules
    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  dynamic "egress" {
    for_each = var.egress_rules
    content {
      from_port   = egress.value.from_port
      to_port     = egress.value.to_port
      protocol    = egress.value.protocol
      cidr_blocks = egress.value.cidr_blocks
    }
  }
}

#------------------------------------------------------------------------------
# Private Security Group 1
#------------------------------------------------------------------------------
resource "aws_security_group" "private" {
  name        = "${var.prefix}-${var.private_sg_name}"
  description = "Example security group 2"
  vpc_id      = var.vpc_id

  dynamic "ingress" {
    for_each = var.private_ingress_rules
    content {
      from_port       = ingress.value.from_port
      to_port         = ingress.value.to_port
      protocol        = ingress.value.protocol
      security_groups = ingress.value.security_groups
    }
  }

  dynamic "egress" {
    for_each = var.private_egress_rules
    content {
      from_port   = egress.value.from_port
      to_port     = egress.value.to_port
      protocol    = egress.value.protocol
      cidr_blocks = egress.value.cidr_blocks
    }
  }

  # Ingress rule to allow traffic from the first security group (sg1) on port 22 (SSH)
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.this.id]
  }
  depends_on = [aws_security_group.this]
}
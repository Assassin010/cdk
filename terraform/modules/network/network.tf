# resource "aws_vpc" "vpc" {
#   for_each = var.vpc_configurations

#   cidr_block             = each.value.cidr_block
#   enable_dns_support     = each.value.enable_dns_support
#   enable_dns_hostnames   = each.value.enable_dns_hostnames

#   tags = {
#     Name = each.value.name
#   }
# }


# resource "aws_subnet" "ansible_private_subnet" {
#   for_each = var.subnet_configurations

#   vpc_id                  = aws_vpc.vpc[each.value.vpc_key].id
#   cidr_block              = each.value.cidr_block
#   availability_zone       = each.value.availability_zone
#   map_public_ip_on_launch = each.value.map_public_ip_on_launch

#   tags = {
#     Name = each.value.name
#   }
# }

# resource "aws_internet_gateway" "igw" {
#   vpc_id = aws_vpc.vpc[var.vpc_key].id

#   tags = {
#     Name = var.igw_name
#   }
# }


# resource "aws_route_table" "public_subnet_route_table" {
#   for_each = var.route_table_configurations

#   vpc_id = aws_vpc.vpc_ansible[var.vpc_key].id

#   route {
#     cidr_block = each.value.cidr_block
#     gateway_id = aws_internet_gateway.igw[var.vpc_key].id
#   }

#   tags = {
#     Name = each.value.name
#   }
# }


# resource "aws_route_table_association" "public_subnet_association" {
#   for_each = var.route_table_association_configurations

#   subnet_id      = aws_subnet.ansible_public_subnet[each.value.subnet_key].id
#   route_table_id = aws_route_table.public_subnet_route_table[each.value.route_table_key].id
# }



module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  name   = var.vpc_name
  cidr   = var.cidr

  azs                  = var.azs
  private_subnets      = var.private_subnets
  public_subnets       = var.public_subnets
  public_subnet_names  = var.public_subnet_names
  private_subnet_names = var.private_subnet_names

  enable_nat_gateway = var.enable_nat_gateway
  create_igw         = var.create_igw
  enable_vpn_gateway = var.enable_vpn_gateway

  #Single NAT Gateway
  single_nat_gateway     = false
  one_nat_gateway_per_az = false
}



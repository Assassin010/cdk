variable "vpc_name" {}
variable "cidr" {}
variable "azs" {}
variable "private_subnets" {}
variable "public_subnets" {}
variable "enable_nat_gateway" {}
variable "enable_vpn_gateway" {}
variable "public_subnet_names" {}
variable "private_subnet_names" {}
variable "create_igw" {}
variable "single_nat_gateway" {}
variable "one_nat_gateway_per_az" {}


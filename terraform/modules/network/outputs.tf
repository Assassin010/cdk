# output "public_subnet_ids" {
#   value = {
#     "Public" = aws_subnet.ansible_public_subnet.id
#   }
# }

# output "private_subnet_ids" {
#   value = {
#     "Private-1" = aws_subnet.ansible_private_subnet_1.id,
#     "Private-2" = aws_subnet.ansible_private_subnet_2.id
#   }
# }

# output "vpc_id" {
#   value = aws_vpc.vpc.id
# }


output "vpc_id" {
  value = module.vpc.vpc_id
}

output "private_subnet_ids" {
  value = {
    "Private-1" = module.vpc.private_subnets[0]
    "Private-2" = module.vpc.private_subnets[1]
  }
}


output "public_subnet_ids" {
  value = {
    "Public-1" = module.vpc.public_subnets[0]
    "Public-2" = module.vpc.public_subnets[1]
  }
}

output "vpc_arn" {
  value = module.vpc.vpc_arn
}

output "vpc_cidr_block" {
  value = module.vpc.vpc_cidr_block
}

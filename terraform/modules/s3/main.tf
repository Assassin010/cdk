resource "aws_s3_bucket" "this" {
  bucket = "${var.prefix}-${var.bucket_name}"

  tags = {
    Name        = "${var.prefix}-${var.bucket_name}"
    Environment = "Dev"
  }
}


resource "aws_s3_bucket_ownership_controls" "this" {
  bucket = aws_s3_bucket.this.id
  rule {
    object_ownership = var.object_ownership
  }
}

resource "aws_s3_bucket_acl" "this" {
  depends_on = [aws_s3_bucket_ownership_controls.this]

  bucket = aws_s3_bucket.this.id
  acl    = var.acl
}
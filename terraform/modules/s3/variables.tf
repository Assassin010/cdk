variable "bucket_name" {
  description = "The name of the S3 bucket"
  type        = string
}

variable "prefix" {
  description = "The prefix to use for the S3 bucket"
  type        = string
}

variable "acl" {
  description = "The canned ACL to apply to the S3 bucket"
  type        = string
}

variable "object_ownership" {
  description = "The object ownership to apply to the S3 bucket"
  type        = string
}
module "ec2_instances" {
  source = "./modules/ec2"

  instances = {
    master_instance = {                                      # Bastion host
      ami                         = "ami-0c24ee2a1e3b9df45", # CentOS7_Minimal_x86_64_20221106-013b4753-6b5f-4e7e-82e9-45dc55d36a7d
      instance_type               = "t2.micro",
      iam_instance_profile        = module.iam.instance_profile_name,
      key_name                    = "cdk-eu-west-1",
      subnet_id                   = module.network.public_subnet_ids["Public-1"],
      security_group_id           = module.ec2_security_group.security_group_id,
      user_data                   = var.instance_user_data,
      associate_public_ip_address = true,
      tag_name                    = "master_server_Amazon_Linux_2023_AMI"
    },

    host_instance_1 = {
      ami           = "ami-0c24ee2a1e3b9df45", # CentOS7
      instance_type = "t2.micro",
      # iam_instance_profile        = "", # Set a default value with empty string yo not attach the role or you can set it to null
      iam_instance_profile        = module.iam.instance_profile_name,
      key_name                    = "cdk-eu-west-1",
      subnet_id                   = module.network.private_subnet_ids["Private-1"],
      security_group_id           = module.ec2_security_group.private_security_group_id,
      user_data                   = var.instance_user_data,
      associate_public_ip_address = false, 
      tag_name                    = "master_server_Amazon_Linux_2023_AMI"
    }

    # host_instance_2 = {
    #   ami                         = "ami-07355fe79b493752d", # Amazon Linux 2023 AMI 
    #   instance_type               = "t2.micro",
    #   iam_instance_profile        = "",
    #   key_name                    = "test-ssm",
    #   subnet_id                   = module.network.private_subnet_ids["Private-1"],
    #   security_group_id           = module.ec2_security_group.private_security_group_id,
    #   user_data                   = var.instance_user_data,
    #   associate_public_ip_address = false, 
    #   tag_name                    = "Amazon Linux 2023"
    # },

    # host_instance_3 = {
    #   ami                         = "ami-07024fbdfd1aab8a0", # debian-12-amd64-20230711-1438
    #   instance_type               = "t2.micro",
    #   iam_instance_profile        = "",
    #   key_name                    = "test-ssm",
    #   subnet_id                   = module.network.private_subnet_ids["Private-1"],
    #   security_group_id           = module.ec2_security_group.private_security_group_id,
    #   user_data                   = var.instance_user_data,
    #   associate_public_ip_address = false, 
    #   tag_name                    = "debian-12"
    # },

    # host_instance_4 = {
    #   ami                         = "ami-0ba6c68ec6fccb204", # debian-11-amd64-20220328-962
    #   instance_type               = "t2.micro",
    #   iam_instance_profile        = "",
    #   key_name                    = "test-ssm",
    #   subnet_id                   = module.network.private_subnet_ids["Private-1"],
    #   security_group_id           = module.ec2_security_group.private_security_group_id,
    #   user_data                   = var.instance_user_data,
    #   associate_public_ip_address = false, 
    #   tag_name                    = "debian-11"
    # },

    # host_instance_5 = {
    #   ami                         = "ami-013d87f7217614e10", # RHEL-9.2.0_HVM-20230503-x86_64-41-Hourly2-GP2
    #   instance_type               = "t2.micro",
    #   iam_instance_profile        = module.iam.instance_profile_name,
    #   key_name                    = "test-ssm",
    #   subnet_id                   = module.network.private_subnet_ids["Private-2"],
    #   security_group_id           = module.ec2_security_group.private_security_group_id,
    #   user_data                   = var.instance_user_data,
    #   associate_public_ip_address = false, 
    #   tag_name                    = "RHEL-9"

    # },

    # host_instance_6 = {
    #   ami                         = "ami-0fa2f7b35eeb82b7a", # RHEL-8.7.0_HVM-20230330-x86_64-56-Hourly2-GP2
    #   instance_type               = "t2.micro",
    #   iam_instance_profile        = module.iam.instance_profile_name,
    #   key_name                    = "test-ssm",
    #   subnet_id                   = module.network.private_subnet_ids["Private-2"],
    #   security_group_id           = module.ec2_security_group.private_security_group_id,
    #   user_data                   = var.instance_user_data,
    #   associate_public_ip_address = false, 
    #   tag_name                    = "RHEL-8.7"
    # },

    # host_instance_7 = {
    #   ami                         = "ami-01dd271720c1ba44f", # ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20230516 22.04 LTS
    #   instance_type               = "t2.micro",
    #   iam_instance_profile        = module.iam.instance_profile_name,
    #   key_name                    = "test-ssm",
    #   subnet_id                   = module.network.private_subnet_ids["Private-2"],
    #   security_group_id           = module.ec2_security_group.private_security_group_id,
    #   user_data                   = var.instance_user_data,
    #   associate_public_ip_address = false, 
    #   tag_name                    = "ubuntu-22.04"
    # },

    # host_instance_8 = {
    #   ami                         = "ami-01dd271720c1ba44f", # ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20230516 22.04 LTS
    #   instance_type               = "t2.micro",
    #   iam_instance_profile        = module.iam.instance_profile_name,
    #   key_name                    = "test-ssm",
    #   subnet_id                   = module.network.private_subnet_ids["Private-1"],
    #   subnet_id                   = module.network.private_subnet_ids["Private-2"],
    #   security_group_id           = module.ec2_security_group.private_security_group_id,
    #   user_data                   = var.instance_user_data,
    #   associate_public_ip_address = false, 
    #   tag_name                    = "ubuntu-22.04"
    # }
  }
  depends_on = [module.ec2_security_group, module.iam, module.network]
}



module "ec2_security_group" {
  source  = "./modules/sg"
  vpc_id  = module.network.vpc_id
  sg_name = "bashion-host-sg"
  prefix  = "cdk"
  ingress_rules = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    },
  ]

  egress_rules = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 0
      to_port     = 0
      protocol    = "icmp"
      cidr_blocks = ["0.0.0.0/0"]
    },
  ]
  private_sg_name = "private-sg"
  private_ingress_rules = [
    {
      from_port       = 22
      to_port         = 22
      protocol        = "tcp"
      security_groups = [module.ec2_security_group.security_group_id]
    },
  ]
  private_egress_rules = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    },
  ]
  depends_on = [module.network]
}

module "s3" {
  source           = "./modules/s3"
  bucket_name      = "ecs"
  object_ownership = "BucketOwnerPreferred"
  acl              = "private"
  prefix           = "cdk-demo"
}


module "iam" {
  source                = "./modules/iam"
  role_name             = "ssm-access-role"
  instance_profile_name = "ec2-instance-profile"
  policy_name           = "ec2-access-policy"
  prefix                = "cdk"
}



module "network" {
  source                 = "./modules/network"
  vpc_name               = "cdk-vpc"
  cidr                   = "10.0.0.0/16"
  azs                    = ["eu-west-1a", "eu-west-1b"]
  private_subnets        = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets         = ["10.0.101.0/24", "10.0.102.0/24"]
  public_subnet_names    = ["Public-1", "Public-2"]
  private_subnet_names   = ["Private-1", "Private-2"]
  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false
  create_igw             = true
  enable_vpn_gateway     = false
}

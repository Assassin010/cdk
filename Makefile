.PHONY: all build run tag push 

IMAGE_NAME = assassin010/zenika-plus-docker
TAG = zenika-plus-docker

all: build run tag push

build:
	docker build -t $(IMAGE_NAME):$(TAG) .

run:
	docker run -it $(IMAGE_NAME):$(TAG)

tag:
	docker tag $(IMAGE_NAME):$(TAG) $(IMAGE_NAME):$(TAG)

push:
	docker push $(IMAGE_NAME):$(TAG)